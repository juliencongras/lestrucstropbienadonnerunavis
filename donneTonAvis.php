<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Donne ton avis !</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@shoelace-style/shoelace@2.4.0/dist/themes/light.css" />
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php 
        include_once("pdo.php");
        include_once("debug.php");

        $currentTruc = $_POST['truc'];

        $req = $pdo->prepare('select * from avis where idTruc = ?;');
        $req->execute([$currentTruc]);
        $infoCT = $req->fetchAll();

        $reqTwo = $pdo->prepare('select * from truc where id = ?;');
        $reqTwo->execute([$currentTruc]);
        $trucInfos = $reqTwo->fetch();
    ?>

    <a href="index.php">Retour</a>

    <div id="trucContainerDTA">
        <img src="images/<?= $trucInfos['image'] ?>" alt="<?= $trucInfos['nom'] ?>">
        <div>
            <h2><?= $trucInfos['nom'] ?></h2>
            <sl-rating label="Rating" readonly value="5"></sl-rating>
        </div>
    </div>

    <form action="test.php" id="formDTA" method="post">

        <sl-rating onclick="getAttributeRating()" id="noteDTA" label="Donner une note à ce truc génial !"></sl-rating>
        <textarea name="texteDTA" id="" cols="40" rows="5"></textarea>
        <input type="hidden" name="idTruc" value="<?= $currentTruc ?>">
        <button>Note</button>

    </form>

    <?php
        foreach($infoCT as $info){
    ?>
        <div class="avisPageDTA">
            <sl-rating label="Rating" readonly value="<?= $info['note'] ?>"></sl-rating>
            <p><?= $info['texte'] ?></p>
        </div>

    <?php } ?>
    <script type="module" src="https://cdn.jsdelivr.net/npm/@shoelace-style/shoelace@2.4.0/dist/shoelace-autoloader.js"></script>
    <script src="script.js"></script>
</body>
</html>