<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Donner son avis sur tout !</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@shoelace-style/shoelace@2.4.0/dist/themes/light.css" />
    <link rel="stylesheet" href="style.css">
</head>
<body>

    <h1>Donne ton avis !</h1>

    <div id="avisContainer">
        <?php 
        include_once("pdo.php");
        include_once("debug.php");

        $reqAvg = $pdo->query('select truc.id, truc.image, truc.nom, round(avg(avis.note), 1) as moyenne from truc inner join avis on avis.idTruc = truc.id group by truc.id;');
        $toutAvis = $reqAvg->fetchAll();

        foreach($toutAvis as $avis){ ?>

            <sl-card class="card-overview" onclick="getTrucID(`truc<?= $avis['id'] ?>`)">
              <img
                slot="image"
                src="images/<?= $avis['image'] ?>"
                alt="<?= $avis['nom'] ?>"
              />

            <strong><?= $avis['nom'] ?></strong><br />

            <div slot="footer">
                <sl-rating label="Rating" readonly value="<?= $avis['moyenne'] ?>"></sl-rating>
            </div>
            <form action="donneTonAvis.php" id="truc<?= $avis['id'] ?>" method="post">
                <input type="hidden" name="truc" value="<?= $avis['id'] ?>">
            </form>

            </sl-card>            

        <?php }
        ?>
    </div>

<script type="module" src="https://cdn.jsdelivr.net/npm/@shoelace-style/shoelace@2.4.0/dist/shoelace-autoloader.js"></script>
<script src="script.js"></script>
</body>
</html>

<style>
    .card-overview {
        max-width: 300px;
    }
    .card-overview:hover{
        cursor: pointer;
    }
    card-overview [slot='footer'] {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
</style>