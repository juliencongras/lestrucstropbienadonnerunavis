drop database if exists dsast;
create database dsast;
use dsast;

create table truc(
    id int auto_increment primary key,
    nom varchar(255) not null,
    image varchar(255) not null
);

create table avis(
    idTruc int,
    texte varchar(255),
    note int not null
);

INSERT INTO truc (nom, image) VALUES 
('Pigeon', 'pigeon.jpeg'),
('Vélo', 'velo.jpg'),
('Toaster à patisseries', 'stupid_product_1.png'),
('Stockeur de fruits', 'stupid_product_2.png'),
('Boîte à céréales', 'stupid_product_3.png'),
('Mixeur de fruits', 'stupid_product_4.png');

INSERT INTO avis (idTruc, texte, note) VALUES 
(1, "C'est génial j'adore les pigeons !", 5),(1, null, 4),(2, null, 4),(2, null, 4),
(3, null, 4),(3, null, 2),(4, null, 2),(4, null, 2),(5, null, 0), (5, null, 5),
(6, null, 5);

drop user if exists machin@'127.0.0.1';
create user machin@'127.0.0.1' identified by 'mdp123';
grant all privileges on dsast.* to machin@'127.0.0.1';